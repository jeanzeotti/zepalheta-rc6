***Settings***
Documentation       Representação do Toaster que mostra as mensagens no sistema

***Variables***
${TOASTER_SUCESS}    css:div[type=sucCess] strong
${TOASTER_ERROR_P}   css:div[type=error] p
${TOASTER_ERROR}     css:div[type=error] strong