***Settings***
Documentation       Login

Resource        ../resources/base.robot

# Executa antes de iniciar cada caso teste
Test Setup      Start Session

# Executa ao finalizar cada caso de teste
Test Teardown   Finish Session

***Test Cases***
Login do Administrador
    [Tags]      smoke
    Acesso á pagina Login
    Submeto minhas credenciais  admin@zepalheta.com.br  pwd123
    Devo ver a área logada